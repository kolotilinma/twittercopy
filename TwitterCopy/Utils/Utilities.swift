//
//  Utilities.swift
//  TwitterCopy
//
//  Created by Михаил Колотилин on 23.05.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit

class Utilities {
    func inputContainerView(withImage image: UIImage, textField: UITextField) -> UIView {
        let view = UIView()
        let imageView = UIImageView()
        let separetor = UIView()
        view.heightAnchor.constraint(equalToConstant: 50).isActive = true
        view.addSubview(imageView)
        view.addSubview(textField)
        view.addSubview(separetor)
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = .white
        imageView.anchor(left: view.leftAnchor, bottom: view.bottomAnchor,
                         paddingBottom: 8, width: 24, height: 24)
        textField.anchor(left: imageView.rightAnchor, bottom: view.bottomAnchor,
                         right: view.rightAnchor, paddingLeft: 8, paddingBottom: 8)
        separetor.backgroundColor = .white
        separetor.anchor(left: view.leftAnchor, bottom: view.bottomAnchor,
                         right: view.rightAnchor, height: 0.75)
        return view
    }
    
    func textField(withPlaceholder placeholder: String) -> UITextField {
        let textField = UITextField()
        textField.textColor = .white
        textField.font = UIFont.systemFont(ofSize: 16)
        textField.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        return textField
    }
    
    func attributedButton(_ firstPart: String, _ secondPart: String) -> UIButton {
        let button = UIButton(type: .system)
        let attributedTitle = NSMutableAttributedString(string: firstPart, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16) ,NSAttributedString.Key.foregroundColor: UIColor.white])
        attributedTitle.append(NSAttributedString(string: secondPart, attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16) ,NSAttributedString.Key.foregroundColor: UIColor.white]))
        button.setAttributedTitle(attributedTitle, for: .normal)
        return button
    }
}
