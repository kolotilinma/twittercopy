//
//  AuthService.swift
//  TwitterCopy
//
//  Created by Михаил Колотилин on 24.05.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

struct AuthCredentials {
    let email: String
    let password: String
    let fullname: String
    let username: String
    let profileImage: UIImage
}

struct AuthService {
    static let shared = AuthService()
    
    func registerUser(credentials: AuthCredentials, completion: @escaping (Result<DatabaseReference, Error>)  -> Void) {
        let email = credentials.email
        let password = credentials.password
        let username = credentials.username
        let fullname = credentials.fullname
        guard let imageData = credentials.profileImage.jpegData(compressionQuality: 0.3) else { return }
        let filename = NSUUID().uuidString
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        let storageRef = STORAGE_PROFILE_IMAGE.child(filename)
        storageRef.putData(imageData, metadata: metadata) { (meta, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            storageRef.downloadURL { (url, error) in
                if let error = error {
                    completion(.failure(error))
                    return
                }
                guard let profileImageUrl = url?.absoluteString else { return }
                
                Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
                    if let error = error {
                        completion(.failure(error))
                        return
                    }
                    guard let uid = result?.user.uid else { return }
                    let values = ["email" : email,
                                  "username": username,
                                  "fullname": fullname,
                                  "profileImageUrl": profileImageUrl]
                    
                    REF_USERS.child(uid).updateChildValues(values) { (error, ref) in
                        if let error = error {
                            completion(.failure(error))
                            return
                        }
                        completion(.success(ref))
                    }
                }
            }
        }
    }
    
    func logUserIn(withEmail email: String, password: String, completion: @escaping (Result<AuthDataResult, Error>)  -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            guard let result = result else { return }
            completion(.success(result))
        }
    }
    
}
