//
//  ConversationsController.swift
//  TwitterCopy
//
//  Created by Михаил Колотилин on 23.05.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit

class ConversationsController: UIViewController {
    
    //MARK: - Properties
    
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        
    }
    
    //MARK: - Helpers
    fileprivate func configureUI() {
        view.backgroundColor = .white
        navigationItem.title = "Messages"
        
        
    }
    
    
    
}
