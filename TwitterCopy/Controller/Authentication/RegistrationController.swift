//
//  RegistrationController.swift
//  TwitterCopy
//
//  Created by Михаил Колотилин on 23.05.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit


class RegistrationController: UIViewController {
    
    //MARK: - Properties
    fileprivate let imagePicker = UIImagePickerController()
    fileprivate var profileImage: UIImage?
    fileprivate let plusPhotoButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "plus_photo"), for: .normal)
        button.tintColor = .white
        button.addTarget(self, action: #selector(handleAddProfilePhoto), for: .touchUpInside)
        return button
    }()
    
    fileprivate lazy var emailContainerView: UIView = {
        let view = Utilities().inputContainerView(withImage: UIImage(systemName: "envelope")!,
                                                  textField: emailTextField)
        return view
    }()
    
    fileprivate lazy var passwordContainerView: UIView = {
        let view = Utilities().inputContainerView(withImage: UIImage(systemName: "lock")!,
                                                  textField: passwordTextField)
        return view
    }()
    
    fileprivate lazy var fullnameContainerView: UIView = {
        let view = Utilities().inputContainerView(withImage: UIImage(systemName: "person")!,
                                                  textField: fullnameTextField)
        return view
    }()
    
    fileprivate lazy var usernameContainerView: UIView = {
        let view = Utilities().inputContainerView(withImage: UIImage(systemName: "person")!,
                                                  textField: usernameTextField)
        return view
    }()
    
    fileprivate let emailTextField: UITextField = {
        let textField = Utilities().textField(withPlaceholder: "Email")
        return textField
    }()
    
    fileprivate let passwordTextField: UITextField = {
        let textField = Utilities().textField(withPlaceholder: "Password")
        textField.isSecureTextEntry = true
        return textField
    }()
    
    fileprivate let fullnameTextField: UITextField = {
        let textField = Utilities().textField(withPlaceholder: "Full name")
        return textField
    }()
    
    fileprivate let usernameTextField: UITextField = {
        let textField = Utilities().textField(withPlaceholder: "Username")
        return textField
    }()
    
    fileprivate let registrationButton: UIButton = {
        let button  = UIButton(type: .system)
        button.setTitle("Sign Up", for: .normal)
        button.setTitleColor(.twitterBlue, for: .normal)
        button.backgroundColor = .white
        button.layer.cornerRadius = 5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.addTarget(self, action: #selector(handleRegistration), for: .touchUpInside)
        return button
    }()
    
    fileprivate let alreadyHaveAccountButton: UIButton = {
        let button = Utilities().attributedButton("Already have an account ", "Log In")
        button.addTarget(self, action: #selector(handleShowLogin), for: .touchUpInside)
        return button
    }()
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        
    }
    
    //MARK: - Selectors
    
    @objc func handleAddProfilePhoto() {
        print(#function)
        present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func handleRegistration() {
        print(#function)
        guard let profileImage = profileImage else {
            print("DEBUG: Please select a profile image")
            return
        }
        guard let email = emailTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        guard let fullname = fullnameTextField.text else { return }
        guard let username = usernameTextField.text?.lowercased() else { return }
        let credentials = AuthCredentials(email: email, password: password, fullname: fullname, username: username, profileImage: profileImage)
        AuthService.shared.registerUser(credentials: credentials) { (result) in
            switch result {
            case .success(_):
                print("DEBUG: Seccessfuly updated user information")
                guard let window = UIApplication.shared.windows.first(where: { $0.isKeyWindow}) else { return }
                guard let tab = window.rootViewController as? MainTabController else { return }
                tab.authenticateUserAndConfigureUI()
                self.dismiss(animated: true, completion: nil)
                
            case .failure(let error):
                print("DEBUG: Error is \(error.localizedDescription)")

            }
        }
        
    }
    
    @objc func handleShowLogin() {
        print(#function)
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Helpers
    fileprivate func configureUI() {
        view.backgroundColor = .twitterBlue
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        view.addSubview(plusPhotoButton)
        plusPhotoButton.centerX(inView: view, topAnchor: view.safeAreaLayoutGuide.topAnchor, paddingTop: 10)
        plusPhotoButton.setDimensions(width: 128, height: 128)
        let stack = UIStackView(arrangedSubviews: [emailContainerView,
                                                   passwordContainerView,
                                                   fullnameContainerView,
                                                   usernameContainerView,
                                                   registrationButton])
        stack.axis = .vertical
        stack.spacing = 20
        stack.distribution = .fillEqually
        view.addSubview(stack)
        stack.anchor(top: plusPhotoButton.bottomAnchor, left: view.leftAnchor,
                     right: view.rightAnchor, paddingTop: 30, paddingLeft: 32, paddingRight: 32)
        
        view.addSubview(alreadyHaveAccountButton)
        alreadyHaveAccountButton.anchor(left: view.leftAnchor,
                                     bottom: view.safeAreaLayoutGuide.bottomAnchor,
                                     right: view.rightAnchor,
                                     paddingLeft: 40, paddingBottom: 6, paddingRight: 40)
    }
}


extension RegistrationController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let profileImage = info[.editedImage] as? UIImage else { return }
        self.profileImage = profileImage
        plusPhotoButton.layer.cornerRadius = 64
        plusPhotoButton.layer.masksToBounds = true
        plusPhotoButton.imageView?.contentMode = .scaleAspectFill
        plusPhotoButton.imageView?.clipsToBounds = true
        plusPhotoButton.layer.borderColor = UIColor.white.cgColor
        plusPhotoButton.layer.borderWidth = 2
        self.plusPhotoButton.setImage(profileImage.withRenderingMode(.alwaysOriginal), for: .normal)
        dismiss(animated: true, completion: nil)
    }
}
