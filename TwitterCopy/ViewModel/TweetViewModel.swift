//
//  TweetViewModel.swift
//  TwitterCopy
//
//  Created by Михаил Колотилин on 27.05.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit

struct TweetViewModel {
    
    let tweet: Tweet
    let user: User
    var profileImageURL: URL? {
        guard let url = URL(string: user.profileImageUrl) else { return URL(string: "") }
        return url
    }
    var timestamp: String {
        let formater = DateComponentsFormatter()
        formater.allowedUnits = [.second, .minute, .hour, .day, .weekOfMonth, .month]
        formater.maximumUnitCount = 2
        formater.unitsStyle = .abbreviated
        let now = Date()
        return formater.string(from: tweet.timestamp, to: now) ?? ""
    }
    var userInfoText: NSAttributedString {
        let title = NSMutableAttributedString(string: user.fullname,
                                       attributes: [.font: UIFont.boldSystemFont(ofSize: 14)])
        title.append(NSAttributedString(string: " @\(user.username)",
                                        attributes: [.font: UIFont.systemFont(ofSize: 14),
                                                     .foregroundColor: UIColor.lightGray]))
        title.append(NSAttributedString(string: " ∙ \(timestamp)",
                                        attributes: [.font: UIFont.systemFont(ofSize: 14),
                                                     .foregroundColor: UIColor.lightGray]))
        return title
    }
    
    init(tweet: Tweet) {
        self.tweet = tweet
        self.user = tweet.user
    }
    
}
